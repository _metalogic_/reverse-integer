package main

// Ref: https://alkeshghorpade.me/post/leetcode-reverse-integer

import (
	"fmt"
	"math"
)

func main() {
	fmt.Printf("The reverse of 123456789 is %d\n", reverse(123456789))
	fmt.Printf("The reverse of 999999999999999 is %d\n", reverse(999999999999999))
}

func reverse(x int) int {
	reverse, reminder := 0, 0

	for x != 0 {
		reminder = x % 10
		reverse = reverse*10 + reminder
		x /= 10
	}

	if reverse > math.MaxInt32 || reverse < math.MinInt32 {
		return 0
	}

	return reverse
}
